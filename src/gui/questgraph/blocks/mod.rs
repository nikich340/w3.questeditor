//
// questgraph::blocks
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
// state
// ----------------------------------------------------------------------------
pub(in gui::questgraph) struct PropertyEditorState {
    blockid: BlockId,
    blocktype: ImString,
    blockname: input::TextField,
    properties: Vec<Property>,
}
// ----------------------------------------------------------------------------
// actions
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub(in gui) enum BlockEditorAction {
    InitFromBlock(BlockId),
    UpdateField(properties::PropertyId, FieldValue),
    Property(properties::PropertyId, properties::PropertyAction),
    DiscardChanges,
    SaveProperties,
    SavePosition(BlockId),
}
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub(in gui) enum BlockAction {
    OnAddInSocket(BlockId),
    OnAddOutSocket(BlockId),
    AddInSocket(BlockId, InSocketId),
    AddOutSocket(BlockId, OutSocketId),
    DeleteInSocket(BlockId, InSocketId),
    DeleteOutSocket(BlockId, OutSocketId),
    ScriptResultBranch(BlockId, bool),
}
// ----------------------------------------------------------------------------
// block creation actions
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub(in gui) enum BlockTemplate {
    SegmentIn,
    SegmentOut,
    SubSegment,
    ShowLayers,
    HideLayers,
    ShowAndHideLayers,
    Scene,
    Interaction,
    WaitUntil(ConditionType),
    Script(String),
    Spawn,
    Despawn,
    SpawnAndDespawn,
    JournalEntry,
    JournalObjective,
    JournalMappin,
    JournalPhaseObjectives,
    JournalQuestOutcome,
    PauseTime,
    UnpauseTime,
    ShiftTime,
    SetTime,
    AddFact,
    Teleport,
    ChangeWorld,
    Reward,
    Randomize,
}
// ----------------------------------------------------------------------------
#[derive(Debug, PartialEq)]
pub(in gui) enum ConditionType {
    Fact,
    MultiOutFact,
    Area(AreaConditionType),
    Time(TimeConditionType),
    Interaction(InteractionConditionType),
    LoadScreen(LoadScreenConditionType),
    Logic(LogicOperation),
}
// ----------------------------------------------------------------------------
#[derive(Debug, PartialEq, Clone)]
pub(in gui) enum AreaConditionType {
    Entered,
    Left,
    Inside,
    Outside,
}
// ----------------------------------------------------------------------------
#[derive(Debug, PartialEq, Clone)]
pub(in gui) enum LoadScreenConditionType {
    Shown,
    Hidden,
}
// ----------------------------------------------------------------------------
#[derive(Debug, PartialEq, Clone)]
pub(in gui) enum TimeConditionType {
    Range,
    After,
    Before,
    Elapsed,
}
// ----------------------------------------------------------------------------
#[derive(Debug, PartialEq, Clone)]
pub(in gui) enum InteractionConditionType {
    Custom,
    Examined,
    Talked,
    Looted,
    Used,
}
// ----------------------------------------------------------------------------
pub(super) struct ScriptTemplate {
    id: String,
    name: ImString,
    function: String,
    params: Vec<ScriptParameter>,
}
// ----------------------------------------------------------------------------
pub(super) enum EditorBlockHint {
    NamedConditions,
    ShowLayers,
    HideLayers,
    ShowAndHideLayers,
    SpawnCommunities,
    DespawnCommunities,
    Spawnsets,
}
// ----------------------------------------------------------------------------
// view
// ----------------------------------------------------------------------------
#[macro_use]
mod properties;

pub(in gui::questgraph) mod cmds;
pub(in gui::questgraph) mod view;
// ----------------------------------------------------------------------------
// action processing
// ----------------------------------------------------------------------------
#[allow(clippy::too_many_arguments)]
#[inline]
pub(super) fn handle_editor_action(
    action: BlockEditorAction,
    state: &mut Option<PropertyEditorState>,
    def: &mut QuestStructure,
    listsprovider: &ListProvider,
    selection: &mut SelectionState,
    planestate: &mut PlaneState,
    blocks: &mut Blocks,
    edges: &mut Edges,
    dirtyflag: &mut bool,
) -> Result<Option<::gui::ActionSequence>, String> {
    update::handle_editor_action(
        action,
        state,
        def,
        listsprovider,
        selection,
        planestate,
        blocks,
        edges,
        dirtyflag,
    )
    .map(|_| None)
}
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn handle_block_action(
    action: BlockAction,
    def: &mut QuestStructure,
    selection: &mut SelectionState,
    planestate: &mut PlaneState,
    blocks: &mut Blocks,
    edges: &mut Edges,
    dirtyflag: &mut bool,
) -> Result<Option<::gui::ActionSequence>, String> {
    update::handle_block_action(action, def, selection, planestate, blocks, edges, dirtyflag)
}
// ----------------------------------------------------------------------------
// misc
// ----------------------------------------------------------------------------
pub(super) const MAX_SOCKETS: usize = 8;

pub(super) use self::graphblocks::EditableBlock as EditableGraphBlock;
pub(super) use self::questblocks::EditableBlock as EditableQuestBlock;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::ImString;
use imgui_controls::input;
use imgui_controls::input::validator;
use imgui_controls::input::Field;

use model::questgraph::primitives::{BlockId, InSocketId, OutSocketId, SegmentId};
use model::questgraph::{QuestBlock, QuestStructure};
use model::shared::{LogicOperation, ScriptParameter};

use self::popup::SocketNameRequest;
use self::properties::Property;

// state
use gui::questgraph::{PlaneState, SelectionState};

// actions

// misc
use super::DataListProvider as ListProvider;
use super::{Block as GraphBlock, Blocks, Edges, ScriptTemplates};
use gui::types::FieldValue;
// ----------------------------------------------------------------------------
type BlockValidationState = Result<(), String>;
// ----------------------------------------------------------------------------
mod update;

mod graphblocks;
mod popup;
mod questblocks;
// ----------------------------------------------------------------------------
/// predefined script block template with function and parameternames + values
impl ScriptTemplate {
    // ------------------------------------------------------------------------
    pub fn new(id: &str, function: &str, params: Vec<ScriptParameter>) -> ScriptTemplate {
        ScriptTemplate {
            id: id.replace(' ', "_").to_lowercase(),
            name: ImString::new(id),
            function: function.to_owned(),
            params,
        }
    }
    // ------------------------------------------------------------------------
    pub fn new_with_caption(
        id: &str,
        caption: &str,
        function: &str,
        params: Vec<ScriptParameter>,
    ) -> ScriptTemplate {
        ScriptTemplate {
            id: id.replace(' ', "_").to_lowercase(),
            name: ImString::new(caption),
            function: function.to_owned(),
            params,
        }
    }
    // ------------------------------------------------------------------------
    pub fn id(&self) -> &str {
        &self.id
    }
    // ------------------------------------------------------------------------
    pub fn name(&self) -> &ImString {
        &self.name
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyEditorState {
    // ------------------------------------------------------------------------
    fn new(blockid: BlockId, properties: Vec<Property>) -> PropertyEditorState {
        let blocktype = ImString::new(blockid.type_caption().to_string());
        let blockname = blockid.name().caption();
        PropertyEditorState {
            blockid,
            blocktype,
            blockname: input::TextField::new("##blockname", Some(blockname))
                .set_label("blockname")
                .set_label_width(properties::view::LABEL_WIDTH)
                .set_validators(vec![
                    validator::is_nonempty(),
                    validator::min_length(2),
                    validator::max_length(128),
                    validator::chars("^[0-9 _a-zA-Z]*$", "[a-z _0-9]"),
                ]),
            properties,
        }
    }
    // ------------------------------------------------------------------------
    fn properties(&self) -> &Vec<Property> {
        &self.properties
    }
    // ------------------------------------------------------------------------
    fn blockname(&self) -> &dyn Field {
        &self.blockname
    }
    // ------------------------------------------------------------------------
    fn blockname_mut(&mut self) -> &mut dyn Field {
        &mut self.blockname
    }
    // ------------------------------------------------------------------------
    pub(super) fn changed(&self) -> bool {
        self.blockname().changed() || self.properties.iter().any(Property::changed)
    }
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.blockname.reset_changed();
        self.properties.iter_mut().for_each(Property::reset_changed);
    }
    // ------------------------------------------------------------------------
    // pub(super) fn valid(&self) -> bool {
    //     self.blockname.valid() && self.properties.iter().all(|p| p.valid())
    // }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// std traits
// ----------------------------------------------------------------------------
use std::ops::Deref;

// ----------------------------------------------------------------------------
impl Deref for EditorBlockHint {
    type Target = str;
    // ------------------------------------------------------------------------
    fn deref(&self) -> &str {
        use self::EditorBlockHint::*;

        match self {
            NamedConditions => "named",
            ShowLayers => "show-only",
            HideLayers => "hide-only",
            ShowAndHideLayers => "both",
            SpawnCommunities => "spawn-only",
            DespawnCommunities => "despawn-only",
            Spawnsets => "both",
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PartialEq<String> for EditorBlockHint {
    // ------------------------------------------------------------------------
    fn eq(&self, other: &String) -> bool {
        self.deref() == other
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Converter
// ----------------------------------------------------------------------------
use super::{Action as QuestGraphAction, MenuAction};

// ----------------------------------------------------------------------------
impl From<BlockAction> for MenuAction {
    fn from(action: BlockAction) -> MenuAction {
        MenuAction::BlockSpecific(action)
    }
}
// ----------------------------------------------------------------------------
impl From<BlockAction> for ::gui::Action {
    fn from(action: BlockAction) -> ::gui::Action {
        ::gui::Action::QuestGraph(QuestGraphAction::Menu(action.into()))
    }
}
// ----------------------------------------------------------------------------
impl From<BlockEditorAction> for QuestGraphAction {
    fn from(action: BlockEditorAction) -> QuestGraphAction {
        QuestGraphAction::QuestBlockEditor(action)
    }
}
// ----------------------------------------------------------------------------
impl From<BlockEditorAction> for ::gui::Action {
    fn from(action: BlockEditorAction) -> ::gui::Action {
        QuestGraphAction::QuestBlockEditor(action).into()
    }
}
// ----------------------------------------------------------------------------
