//
// properties::view::conditions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::{ImStr, Ui};
use imgui_controls::input::Field;

use super::PropertyView;

// actions
use super::PropertyAction;

// state
use super::conds::{
    AreaCondition, Condition, FactCondition, InteractionCondition, LoadScreenCondition,
    LogicConditionSet, NamedCondition, NamedConditionSet, TimeCondition,
};
use super::{
    AreaConditionType, ConditionType, InteractionConditionType, LoadScreenConditionType,
    TimeConditionType,
};

// misc
use super::{AsCaption, AsOptionList};
use model::shared::{CompareFunction, LogicOperation};

use super::LABEL_WIDTH;
// ----------------------------------------------------------------------------
impl PropertyView for FactCondition {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.factid.valid()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed || self.factid.changed() || self.value.changed()
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        let width_op = 13.0;
        let width_value = 200.0;
        let width_fact = ui.get_contentregion_width() - width_op - width_value;

        // workaround for flex width scaling
        let width_remaining =
            if ui.get_contentregion_width() - ui.get_contentregion_avail_width() > 0.0 {
                width_fact - 10.0
            } else {
                width_fact
            };

        if let Some(value) = self.factid.adjust_width(width_remaining).draw(ui) {
            result = Some(PropertyAction::SubfieldUpdate(0, value.into()));
        }

        ui.same_line(0.0);
        if let Some(id) =
            super::draw_popup_selection(ui, &self.compareop, width_op, im_str!("##popup"))
        {
            result = Some(PropertyAction::ConditionSetCompareType(id));
        }

        ui.same_line(0.0);
        if let Some(value) = self.value.draw(ui) {
            result = Some(PropertyAction::SubfieldUpdate(1, value.into()));
        }

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for AreaCondition {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.actor.valid() && self.area.valid()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.actor.changed() || self.area.changed()
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;
        let default_padding = 4.0;

        // control has multiple lines -> check if margin for next lines is required
        let margin = 0.5 * (ui.get_contentregion_width() - ui.get_contentregion_avail_width());

        if margin > 0.0 {
            // margin in *current* line
            ui.same_line(margin + default_padding);
        }
        if let Some(value) = self.actor.draw(ui) {
            result = Some(PropertyAction::SubfieldUpdate(0, value.into()));
        }

        if margin > 0.0 {
            // new_line + same_line(> 0) == margin in next line
            ui.new_line();
            ui.same_line(margin + default_padding);
        }
        if let Some(action) = self.area.draw(ui) {
            result = Some(action);
        }

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for TimeCondition {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.time1.valid() && self.time2.as_ref().map(Field::valid).unwrap_or(true)
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.time1.changed() || self.time2.as_ref().map(Field::changed).unwrap_or(false)
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        if let Some(value) = self.time1.draw(ui) {
            result = Some(PropertyAction::SubfieldUpdate(0, value.into()));
        }

        if let Some(ref mut time2) = self.time2 {
            ui.same_line(0.0);
            ui.text(im_str!("-"));
            ui.same_line(0.0);
            if let Some(value) = time2.draw(ui) {
                result = Some(PropertyAction::SubfieldUpdate(1, value.into()));
            }
        }
        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for InteractionCondition {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.entity.valid() && self.interaction.as_ref().map(Field::valid).unwrap_or(true)
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.entity.changed()
            || self
                .interaction
                .as_ref()
                .map(Field::changed)
                .unwrap_or(false)
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;
        let default_padding = 4.0;

        // control has multiple lines -> check if margin for next lines is required
        let margin = 0.5 * (ui.get_contentregion_width() - ui.get_contentregion_avail_width());

        if margin > 0.0 {
            // margin in *current* line
            ui.same_line(margin + default_padding);
        }

        if let Some(ref mut interaction) = self.interaction {
            if let Some(value) = interaction.draw(ui) {
                result = Some(PropertyAction::SubfieldUpdate(0, value.into()));
            }

            if margin > 0.0 {
                // new_line + same_line(> 0) == margin in next line
                ui.new_line();
                ui.same_line(margin + default_padding);
            }
        }

        if let Some(action) = self.entity.draw(ui) {
            result = Some(action);
        }

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for LoadScreenCondition {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        true
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        false
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        ui.text(im_str!("loadscreen"));
        ui.same_line(LABEL_WIDTH);

        let col = (0.5, 0.5, 0.5, 0.9);

        match self._type {
            LoadScreenConditionType::Shown => ui.text_colored(col, im_str!("shown")),
            LoadScreenConditionType::Hidden => ui.text_colored(col, im_str!("hidden")),
        }
        None
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for Condition {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.control.valid()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed || self.control.changed()
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        if let Some(id) =
            super::draw_type_selection(ui, &self._type, im_str!("##condtype_popup"), None)
        {
            result = Some(PropertyAction::ConditionSetType(id));
        }

        ui.same_line(25.0);
        if let Some(action) = self.control.draw(ui) {
            result = Some(action);
        }
        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for NamedCondition {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.cond.valid()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.cond.changed()
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let result = self.cond.draw(ui);

        let col = (0.5, 0.5, 0.5, 0.9);
        ui.text_colored(col, im_str!("on socket"));
        ui.same_line(LABEL_WIDTH);
        ui.text_colored(col, &self.caption);

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for LogicConditionSet {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.conditions.iter().all(PropertyView::valid)
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed || self.conditions.iter().any(PropertyView::changed)
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        let v_button_width = 15.0;
        let remaining_width = ui.get_contentregion_width() - 8.0;

        // -- label + logic op + add condition
        super::draw_colored_label(ui, im_str!("satisfied conditions: "), self.error.as_ref());
        ui.same_line(0.0);

        if let Some(id) = super::draw_popup_selection(ui, &self.logicop, 90.0, im_str!("##popup")) {
            result = Some(PropertyAction::ConditionListSetLogicType(id));
        }

        if self.conditions.len() < super::MAX_CONDITIONS {
            ui.same_line(remaining_width);
            if ui.small_button(im_str!("+")) {
                result = Some(PropertyAction::ListItemAdd);
            }
        }

        // -- condition list
        ui.columns(2, im_str!("conditions_table"), false);
        ui.set_column_width(0, remaining_width);
        ui.set_column_width(1, v_button_width);

        if !self.conditions.is_empty() {
            ui.separator();
        }

        for (i, cond) in self.conditions.iter_mut().enumerate() {
            ui.with_id(i as i32, || {
                if let Some(action) = cond.draw(ui) {
                    result = Some(PropertyAction::ListItemAction(i, Box::new(action)))
                }
                ui.next_column();
                // workaround for vanishing button on time conditions (same_line must be != 0.0)
                ui.same_line(0.5);

                if ui.button(im_str!("-"), (0.0, super::BUTTON_HEIGHT)) {
                    result = Some(PropertyAction::ListItemDel(i));
                }
                ui.next_column();
            });
        }
        ui.columns(1, im_str!("conditions_table"), false);

        result
    }
}
// ----------------------------------------------------------------------------
impl PropertyView for NamedConditionSet {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.conditions.iter().all(PropertyView::valid)
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed || self.conditions.iter().any(PropertyView::changed)
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        for (i, cond) in self.conditions.iter_mut().enumerate() {
            if i > 0 {
                super::draw_colored_separator(ui, super::COL_SEPARATOR_LIGHT);
            }
            ui.with_id(i as i32, || {
                if let Some(action) = cond.draw(ui) {
                    result = Some(PropertyAction::ListItemAction(i, Box::new(action)))
                }
            });
        }

        result
    }
}
// ----------------------------------------------------------------------------
// Helper
// ----------------------------------------------------------------------------
impl AsCaption for CompareFunction {
    // ------------------------------------------------------------------------
    fn caption(&self) -> &ImStr {
        match self {
            CompareFunction::EQ => im_str!("=="),
            CompareFunction::NEQ => im_str!("!="),
            CompareFunction::LT => im_str!(" <"),
            CompareFunction::LTE => im_str!("<="),
            CompareFunction::GT => im_str!(" >"),
            CompareFunction::GTE => im_str!(">="),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl AsCaption for LogicOperation {
    // ------------------------------------------------------------------------
    fn caption(&self) -> &ImStr {
        match self {
            LogicOperation::AND => im_str!("all"),
            LogicOperation::OR => im_str!("any"),
            LogicOperation::XOR => im_str!("exactly one"),
            LogicOperation::NAND => im_str!("not all"),
            LogicOperation::NOR => im_str!("none"),
            LogicOperation::NXOR => im_str!("nxor"),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl AsOptionList for ConditionType {
    type Id = ConditionType;
    // ------------------------------------------------------------------------
    fn as_short_caption(&self) -> (&str, Self::Id) {
        use self::AreaConditionType::*;
        use self::InteractionConditionType::*;
        use self::LoadScreenConditionType::*;
        use self::TimeConditionType::*;

        match self {
            ConditionType::Fact => ("F", ConditionType::Fact),
            ConditionType::Area(cond) => match cond {
                Entered => ("A", ConditionType::Area(Entered)),
                Left => ("A", ConditionType::Area(Left)),
                Inside => ("A", ConditionType::Area(Inside)),
                Outside => ("A", ConditionType::Area(Outside)),
            },
            ConditionType::Time(cond) => match cond {
                Before => ("T", ConditionType::Time(Before)),
                After => ("T", ConditionType::Time(After)),
                Range => ("T", ConditionType::Time(Range)),
                Elapsed => ("T", ConditionType::Time(Elapsed)),
            },
            ConditionType::Interaction(cond) => match cond {
                Custom => ("I", ConditionType::Interaction(Custom)),
                Examined => ("I", ConditionType::Interaction(Examined)),
                Talked => ("I", ConditionType::Interaction(Talked)),
                Used => ("I", ConditionType::Interaction(Used)),
                Looted => ("I", ConditionType::Interaction(Looted)),
            },
            // unsupported!
            ConditionType::LoadScreen(_) => ("?", ConditionType::LoadScreen(Shown)),
            ConditionType::Logic(_) => ("?", ConditionType::Logic(LogicOperation::AND)),
            ConditionType::MultiOutFact => ("?", ConditionType::Fact),
        }
    }
    // ------------------------------------------------------------------------
    fn options(&self) -> Vec<(&::imgui::ImStr, Self::Id)> {
        use self::AreaConditionType::*;
        use self::ConditionType::*;
        use self::InteractionConditionType::*;
        use self::TimeConditionType::*;

        vec![
            (im_str!("fact"), Fact),
            (im_str!("area: entered"), Area(Entered)),
            (im_str!("area: left"), Area(Left)),
            (im_str!("area: inside"), Area(Inside)),
            (im_str!("area: outside"), Area(Outside)),
            (im_str!("time: before"), Time(Before)),
            (im_str!("time: after"), Time(After)),
            (im_str!("time: between"), Time(Range)),
            (im_str!("time: elapsed realtime"), Time(Elapsed)),
            (im_str!("interaction: examined"), Interaction(Examined)),
            (im_str!("interaction: talked"), Interaction(Talked)),
            (im_str!("interaction: used"), Interaction(Used)),
            // (im_str!("interaction: looted"), Interaction(Looted)),
            (im_str!("interaction: custom"), Interaction(Custom)),
        ]
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl AsOptionList for CompareFunction {
    type Id = CompareFunction;
    // ------------------------------------------------------------------------
    fn as_short_caption(&self) -> (&str, Self::Id) {
        match self {
            CompareFunction::EQ => ("==", CompareFunction::EQ),
            CompareFunction::NEQ => ("!=", CompareFunction::NEQ),
            CompareFunction::LT => (" <", CompareFunction::LT),
            CompareFunction::LTE => ("<=", CompareFunction::LTE),
            CompareFunction::GT => (" >", CompareFunction::GT),
            CompareFunction::GTE => (">=", CompareFunction::GTE),
        }
    }
    // ------------------------------------------------------------------------
    fn options(&self) -> Vec<(&::imgui::ImStr, Self::Id)> {
        vec![
            (im_str!("=="), CompareFunction::EQ),
            (im_str!("!="), CompareFunction::NEQ),
            (im_str!(" <"), CompareFunction::LT),
            (im_str!("<="), CompareFunction::LTE),
            (im_str!(" >"), CompareFunction::GT),
            (im_str!(">="), CompareFunction::GTE),
        ]
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl AsOptionList for LogicOperation {
    type Id = LogicOperation;
    // ------------------------------------------------------------------------
    fn as_short_caption(&self) -> (&str, Self::Id) {
        match self {
            LogicOperation::AND => ("all", LogicOperation::AND),
            LogicOperation::OR => ("any", LogicOperation::OR),
            LogicOperation::XOR => ("exactly one", LogicOperation::XOR),
            LogicOperation::NAND => ("not all", LogicOperation::NAND),
            LogicOperation::NOR => ("none", LogicOperation::NOR),
            LogicOperation::NXOR => ("nxor", LogicOperation::NXOR),
        }
    }
    // ------------------------------------------------------------------------
    fn options(&self) -> Vec<(&::imgui::ImStr, Self::Id)> {
        vec![
            (im_str!("all"), LogicOperation::AND),
            (im_str!("any"), LogicOperation::OR),
            // ----------------------------------------------------------------
            // the following logic condition combinators show unexpected behavior
            // (at least in combination with fact checking). to prevent hard to
            // debug problems the creation in editor is being deactivated
            // (although the encoder still DOES support them and the editor will
            // correctly show them in the graph!)
            // ----------------------------------------------------------------
            // (im_str!("exactly one"), LogicOperation::XOR),
            // (im_str!("not all"), LogicOperation::NAND),
            // (im_str!("none"), LogicOperation::NOR),
            // (im_str!("nxor"), LogicOperation::NXOR),
        ]
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
