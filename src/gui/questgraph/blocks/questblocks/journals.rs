//
// blocks::questblocks::journals
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use imgui_controls::input;

use model::questgraph::journals;
use model::questgraph::journals::JournalElementReference;
use model::questgraph::QuestBlock;

use super::properties;
use super::properties::view::LABEL_WIDTH;
use super::properties::Property;

use super::EditableBlock;
use super::ListProvider;
// ----------------------------------------------------------------------------
impl EditableBlock for journals::JournalEntry {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        vec![
            Property::new_control(
                "##entry",
                Box::new(properties::JournalReference::new(
                    LABEL_WIDTH,
                    listprovider.journal_entries(),
                    self.reference(),
                )),
            ),
            new_property_field_bool!("##notify", "notify", *self.notify()),
            new_property_field_bool!("##include_root", "add root", *self.include_root()),
        ]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Field(id, field) if id.as_str() == "##notify" => {
                    self.set_notify(field.value().as_bool()?)
                }
                Field(id, field) if id.as_str() == "##include_root" => {
                    self.set_include_root(field.value().as_bool()?)
                }
                Control(id, ctl) if id.as_str() == "##entry" => {
                    let entry = ctl_to_property!(ctl, properties::JournalReference, "##entry");

                    self.set_opt_reference(JournalElementReference::try_from(entry).ok());
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for journals::JournalObjective {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        vec![
            Property::new_control(
                "##objective",
                Box::new(properties::JournalObjectiveReference::new(
                    LABEL_WIDTH,
                    listprovider.journal_quests(),
                    self.reference(),
                )),
            ),
            new_property_field_bool!("##notify", "notify", *self.notify()),
            new_property_field_bool!("##track", "track", *self.track()),
        ]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Field(id, field) if id.as_str() == "##notify" => {
                    self.set_notify(field.value().as_bool()?)
                }
                Field(id, field) if id.as_str() == "##track" => {
                    self.set_track(field.value().as_bool()?)
                }
                Control(id, ctl) if id.as_str() == "##objective" => {
                    let objective =
                        ctl_to_property!(ctl, properties::JournalObjectiveReference, "##objective");

                    self.set_opt_reference(JournalElementReference::try_from(objective).ok());
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for journals::JournalPhaseObjectives {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        vec![
            Property::new_control(
                "##phase",
                Box::new(properties::JournalPhaseObjectivesReference::new(
                    LABEL_WIDTH,
                    listprovider.journal_quests(),
                    self.reference(),
                )),
            ),
            new_property_field_bool!("##notify", "notify", *self.notify()),
        ]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Field(id, field) if id.as_str() == "##notify" => {
                    self.set_notify(field.value().as_bool()?)
                }
                Control(id, ctl) if id.as_str() == "##phase" => {
                    let phase = ctl_to_property!(
                        ctl,
                        properties::JournalPhaseObjectivesReference,
                        "##phase"
                    );

                    self.set_opt_reference(JournalElementReference::try_from(phase).ok());
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for journals::JournalQuestOutcome {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        vec![
            Property::new_control(
                "##quest",
                Box::new(properties::JournalQuestOutcomeReference::new(
                    LABEL_WIDTH,
                    listprovider.journal_quests(),
                    self.reference(),
                )),
            ),
            new_property_field_bool!("##notify", "notify", *self.notify()),
        ]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Field(id, field) if id.as_str() == "##notify" => {
                    self.set_notify(field.value().as_bool()?)
                }
                Control(id, ctl) if id.as_str() == "##quest" => {
                    let quest =
                        ctl_to_property!(ctl, properties::JournalQuestOutcomeReference, "##quest");

                    self.set_opt_reference(JournalElementReference::try_from(quest).ok());
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for journals::JournalMappin {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        vec![Property::new_control(
            "##mappin",
            Box::new(properties::JournalMapPinReference::new(
                LABEL_WIDTH,
                listprovider.journal_quests(),
                self.reference(),
            )),
        )]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Control(id, ctl) if id.as_str() == "##mappin" => {
                    let mappin =
                        ctl_to_property!(ctl, properties::JournalMapPinReference, "##mappin");

                    self.set_opt_reference(JournalElementReference::try_from(mappin).ok());
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
