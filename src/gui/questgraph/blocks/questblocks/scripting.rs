//
// blocks::questblocks::scripting
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use imgui_controls::input;
use imgui_controls::input::validator;

use model::questgraph::QuestBlock;
use model::questgraph::scripting;

use model::shared::{ScriptCall, ScriptParameter};

use super::properties;
use super::properties::Property;

use super::ListProvider;
use super::EditableBlock;
// ----------------------------------------------------------------------------
impl EditableBlock for scripting::Script {
    // ------------------------------------------------------------------------
    fn properties(&self, _listprovider: &ListProvider) -> Vec<Property> {
        let no_params = Vec::default();
        let (function_name, params) = self.scriptcall()
            .map(|call| (call.function().as_str(), call.params()))
            .unwrap_or(("", &no_params));
        vec![
            new_property_field!(
                "##function",
                "function",
                function_name,
                input::TextField::new,
                vec![
                    validator::is_nonempty(),
                    validator::chars("^[0-9_a-zA-Z]*$", "[a-z_0-9]"),
                    validator::min_length(2),
                    validator::max_length(250),
                ]
            ),
            Property::new_control(
                "##parameters",
                Box::new(properties::ScriptParameterSet::new(params)),
            ),
        ]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        let mut call = ScriptCall::default();

        for property in properties {
            match property {
                Field(id, field) if id.as_str() == "##function" => {
                    call.set_function(field.value().as_str()?)
                }
                Control(id, ctl) if id.as_str() == "##parameters" => {
                    let p_set =
                        ctl_to_property!(ctl, properties::ScriptParameterSet, "##parameters");

                    for param in p_set.params() {
                        call.add_param(ScriptParameter::try_from(param)?);
                    }
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        self.set_function_call(call);
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
