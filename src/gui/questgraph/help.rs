//
// gui::help
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Hash, Eq, PartialEq, Debug)]
pub enum HelpTopic {
    Graph,
    Block(BlockType),
}
// ----------------------------------------------------------------------------
#[derive(Hash, Eq, PartialEq, Debug)]
pub enum BlockType {
    QuestStart,
    QuestEnd,
    SegmentIn,
    SegmentOut,
    SubSegment,
    Scene,
    Interaction,
    WaitUntil,
    WaitUntilMultiSocket,
    Layers,
    LayersShow,
    LayersHide,
    Script,
    Spawnsets,
    Spawn,
    Despawn,
    JournalEntry,
    JournalObjective,
    JournalMappin,
    JournalPhaseObjectives,
    JournalQuestOutcome,
    PauseTime,
    UnpauseTime,
    ShiftTime,
    SetTime,
    AddFact,
    Reward,
    Teleport,
    ChangeWorld,
    Randomize,
    Invalid,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use super::blocks::EditorBlockHint;
use super::BlockId;
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a str> for HelpTopic {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(id: &str) -> Result<Self, Self::Error> {
        use self::HelpTopic::*;

        match id {
            "graph#usage" => Ok(Graph),
            id if id.starts_with("graph#block#") => Ok(HelpTopic::Block(BlockType::try_from(id)?)),
            _ => Err(String::from("unknown graph topic")),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a str> for BlockType {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(id: &str) -> Result<Self, Self::Error> {
        use self::BlockType::*;

        match id {
            "graph#block#waituntil" => Ok(WaitUntil),
            "graph#block#waituntil#multisocket" => Ok(WaitUntilMultiSocket),
            "graph#block#layers" => Ok(Layers),
            "graph#block#layers#show" => Ok(LayersShow),
            "graph#block#layers#hide" => Ok(LayersHide),
            "graph#block#queststart" => Ok(QuestStart),
            "graph#block#questend" => Ok(QuestEnd),
            "graph#block#segmentin" => Ok(SegmentIn),
            "graph#block#segmentout" => Ok(SegmentOut),
            "graph#block#subsegment" => Ok(SubSegment),
            "graph#block#scene" => Ok(Scene),
            "graph#block#interaction" => Ok(Interaction),
            "graph#block#script" => Ok(Script),
            "graph#block#spawnsets" => Ok(Spawnsets),
            "graph#block#spawn" => Ok(Spawn),
            "graph#block#despawn" => Ok(Despawn),
            "graph#block#journalentry" => Ok(JournalEntry),
            "graph#block#journalobjective" => Ok(JournalObjective),
            "graph#block#journalmappin" => Ok(JournalMappin),
            "graph#block#journalphaseobjectives" => Ok(JournalPhaseObjectives),
            "graph#block#journalquestoutcome" => Ok(JournalQuestOutcome),
            "graph#block#pausetime" => Ok(PauseTime),
            "graph#block#unpausetime" => Ok(UnpauseTime),
            "graph#block#shifttime" => Ok(ShiftTime),
            "graph#block#settime" => Ok(SetTime),
            "graph#block#addfact" => Ok(AddFact),
            "graph#block#reward" => Ok(Reward),
            "graph#block#teleport" => Ok(Teleport),
            "graph#block#changeworld" => Ok(ChangeWorld),
            "graph#block#randomize" => Ok(Randomize),
            _ => Err(String::from("unknown block topic")),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a BlockId> for BlockType {
    // ------------------------------------------------------------------------
    fn from(id: &BlockId) -> BlockType {
        use self::BlockType::*;

        match id {
            BlockId::SubSegment(_) => SubSegment,
            BlockId::QuestStart(_) => QuestStart,
            BlockId::QuestEnd(_) => QuestEnd,
            BlockId::SegmentIn(_) => SegmentIn,
            BlockId::SegmentOut(_) => SegmentOut,
            BlockId::Scene(_) => Scene,
            BlockId::Interaction(_) => Interaction,
            BlockId::WaitUntil(_) => WaitUntil,
            BlockId::Script(_) => Script,
            BlockId::Layers(_) => Layers,
            BlockId::Spawnsets(_) => Spawnsets,
            BlockId::Spawn(_) => Spawn,
            BlockId::Despawn(_) => Despawn,
            BlockId::JournalEntry(_) => JournalEntry,
            BlockId::JournalObjective(_) => JournalObjective,
            BlockId::JournalMappin(_) => JournalMappin,
            BlockId::JournalPhaseObjectives(_) => JournalPhaseObjectives,
            BlockId::JournalQuestOutcome(_) => JournalQuestOutcome,
            BlockId::PauseTime(_) => PauseTime,
            BlockId::UnpauseTime(_) => UnpauseTime,
            BlockId::ShiftTime(_) => ShiftTime,
            BlockId::SetTime(_) => SetTime,
            BlockId::AddFact(_) => AddFact,
            BlockId::Reward(_) => Reward,
            BlockId::Teleport(_) => Teleport,
            BlockId::ChangeWorld(_) => ChangeWorld,
            BlockId::Randomize(_) => Randomize,
            BlockId::Invalid => Invalid,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a BlockId> for HelpTopic {
    // ------------------------------------------------------------------------
    fn from(id: &BlockId) -> HelpTopic {
        HelpTopic::Block(BlockType::from(id))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<(&'a BlockId, Option<&'a EditorBlockHint>)> for HelpTopic {
    // ------------------------------------------------------------------------
    fn from(data: (&BlockId, Option<&EditorBlockHint>)) -> HelpTopic {
        use self::BlockType::*;

        let block_type = match data {
            (BlockId::WaitUntil(_), Some(hint)) => match hint {
                EditorBlockHint::NamedConditions => WaitUntilMultiSocket,
                _ => WaitUntil,
            },
            (BlockId::Layers(_), Some(hint)) => match hint {
                EditorBlockHint::ShowLayers => LayersShow,
                EditorBlockHint::HideLayers => LayersHide,
                _ => Layers,
            },
            (id, _) => BlockType::from(id),
        };
        HelpTopic::Block(block_type)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
