//
// questgraph::popup
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(super) struct SegmentNameRequest {
    pub(super) segmentname: input::TextField,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui_controls::input;
use imgui_controls::input::validator;
use imgui_controls::input::Field;
use imgui_controls::popup::{PopupContent, PopupControl};

// actions
use gui::{Action, PopupAction};
use super::Action as QuestGraphAction;

// state

// misc
use super::SegmentId;

// ----------------------------------------------------------------------------
impl Default for SegmentNameRequest {
    // ------------------------------------------------------------------------
    fn default() -> SegmentNameRequest {
        SegmentNameRequest {
            segmentname: input::TextField::new::<_, &str>("##segmentname", None)
                // .set_label_width(properties::LABEL_WIDTH)
                .set_validators(vec![
                    validator::is_nonempty(),
                    validator::min_length(3),
                    validator::max_length(128),
                    validator::chars("^[0-9_a-z]*$", "[a-z_0-9]"),
                ]),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PopupContent<PopupAction, Action> for SegmentNameRequest {}
// ----------------------------------------------------------------------------
impl PopupControl<PopupAction, Action> for SegmentNameRequest {
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PopupAction) -> Option<Action> {
        match action {
            PopupAction::UpdateField(id, value) => match id.as_str() {
                "segmentname" => {
                    if self.segmentname.validate().valid() {
                        self.segmentname.set_value((&value).into()).ok();
                    }
                }
                unknown => warn!(
                    "update action for unknown popup field [{}]: {}",
                    unknown, value
                ),
            },
        }
        None
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) {
        self.segmentname.validate();
    }
    // ------------------------------------------------------------------------
    fn on_ok(&self) -> (bool, Option<Action>) {
        if self.segmentname.valid() {
            if let Ok(name) = self.segmentname.value().as_str() {
                (
                    true,
                    Some(QuestGraphAction::CreateSegment(SegmentId::from(name)).into()),
                )
            } else {
                (false, None)
            }
        } else {
            (false, None)
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// debug trait impl
// ----------------------------------------------------------------------------
use std::fmt;

impl fmt::Debug for SegmentNameRequest {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "SegmentNameRequest")
    }
}
// ----------------------------------------------------------------------------
