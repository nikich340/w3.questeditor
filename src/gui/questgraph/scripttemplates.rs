//
// blocks::scripttemplates
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(super) fn generate_list(questid: &str) -> ScriptTemplates {
    let mut scripttemplates = IndexMap::new();

    scripttemplates.insert(
        ImString::new(""),
        vec![
            ScriptTemplate::new_with_caption(
                "logMsg",
                "logging",
                "radLog",
                vec![
                    ScriptParameter::new_cname_with("logChannel", questid.to_uppercase()),
                    ScriptParameter::new_string("msg"),
                ],
            ),
        ],
    );

    add_ntr(&mut scripttemplates);
    add_facts(&mut scripttemplates);
    add_noticeboards(&mut scripttemplates);
    add_interaction(&mut scripttemplates);
    add_gameplay(&mut scripttemplates);
    add_environment(&mut scripttemplates);
    add_entities(&mut scripttemplates);
    add_actors(&mut scripttemplates);
    add_items(&mut scripttemplates);
    add_camera(&mut scripttemplates);
    add_hud(&mut scripttemplates);
    add_glossary(&mut scripttemplates);

    scripttemplates
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use indexmap::IndexMap;
use imgui::ImString;

use model::shared::ScriptParameter;

use super::{ScriptTemplate, ScriptTemplates};
// ----------------------------------------------------------------------------
fn add_interaction(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("interaction..."),
        vec![
            ScriptTemplate::new(
                "enable container",
                "EnableOrDisableContainers",
                vec![
                    ScriptParameter::new_cname("containersTag"),
                    ScriptParameter::new_bool_with("containerEnabled", true),
                ],
            ),
            ScriptTemplate::new(
                "disable container",
                "EnableOrDisableContainers",
                vec![
                    ScriptParameter::new_cname("containersTag"),
                    ScriptParameter::new_bool_with("containerEnabled", false),
                ],
            ),
            ScriptTemplate::new(
                "enable teleport",
                "ManageTeleport",
                vec![
                    ScriptParameter::new_cname("teleportTag"),
                    ScriptParameter::new_bool_with("enabling_activating", true),
                    ScriptParameter::new_bool_with("value", true),
                    ScriptParameter::new_bool_with("keepBlackscreen", true),
                    ScriptParameter::new_f32_with("activationTime", 1.0),
                ],
            ),
            ScriptTemplate::new(
                "disable teleport",
                "ManageTeleport",
                vec![
                    ScriptParameter::new_cname("teleportTag"),
                    ScriptParameter::new_bool("enabling_activating"),
                    ScriptParameter::new_bool_with("value", false),
                    ScriptParameter::new_bool_with("keepBlackscreen", true),
                    ScriptParameter::new_f32_with("activationTime", 1.0),
                ],
            ),
            ScriptTemplate::new(
                "open gate",
                "ManageGate",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_bool_with("open", true),
                    ScriptParameter::new_f32_with("speedModifier", 1.0),
                ],
            ),
            ScriptTemplate::new(
                "close gate",
                "ManageGate",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_bool_with("open", false),
                    ScriptParameter::new_f32_with("speedModifier", 1.0),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_entities(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("entities..."),
        vec![
            ScriptTemplate::new_with_caption(
                "add entitytag",
                "add tag",
                "AddTagToEntitiesQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("newTag"),
                    ScriptParameter::new_bool_with("remove", false),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "remove entitytag",
                "remove tag",
                "AddTagToEntitiesQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("newTag"),
                    ScriptParameter::new_bool_with("remove", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "destroy entity",
                "destroy",
                "DestroyEntity",
                vec![ScriptParameter::new_cname("entityTag")],
            ),
            ScriptTemplate::new_with_caption(
                "play entity effect",
                "play effect",
                "PlayEffectQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("effectName"),
                    ScriptParameter::new_bool_with("activate", true),
                    ScriptParameter::new_bool("persistentEffect"),
                    ScriptParameter::new_bool("deactivateAll"),
                    ScriptParameter::new_bool_with("preventEffectStacking", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "stop entity effect",
                "stop effect",
                "PlayEffectQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("effectName"),
                    ScriptParameter::new_bool_with("activate", false),
                    ScriptParameter::new_bool("persistentEffect"),
                    ScriptParameter::new_bool("deactivateAll"),
                    ScriptParameter::new_bool("preventEffectStacking"),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "stop all entity effects",
                "stop all effects",
                "PlayEffectQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("effectName"),
                    ScriptParameter::new_bool_with("activate", false),
                    ScriptParameter::new_bool("persistentEffect"),
                    ScriptParameter::new_bool_with("deactivateAll", true),
                    ScriptParameter::new_bool("preventEffectStacking"),
                ],
            ),
            ScriptTemplate::new(
                "enable component",
                "EntityComponentQuest",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_cname("componentName"),
                    ScriptParameter::new_bool_with("bEnable", true),
                ],
            ),
            ScriptTemplate::new(
                "disable component",
                "EntityComponentQuest",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_cname("componentName"),
                    ScriptParameter::new_bool_with("bEnable", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_actors(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("actors..."),
        vec![
            ScriptTemplate::new(
                "equip item",
                "EquipItemQuest",
                vec![
                    ScriptParameter::new_cname("targetTag"),
                    ScriptParameter::new_cname("itemName"),
                    ScriptParameter::new_cname("itemCategory"),
                    ScriptParameter::new_cname("itemTag"),
                    ScriptParameter::new_bool_with("unequip", true),
                    ScriptParameter::new_bool_with("toHand", true),
                ],
            ),
            ScriptTemplate::new(
                "change appearance",
                "AppearanceChange",
                vec![
                    ScriptParameter::new_cname("npcsTag"),
                    ScriptParameter::new_cname("appearanceName"),
                ],
            ),
            ScriptTemplate::new(
                "assign group attitude",
                "AssignNPCGroupAttitudeQuest",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_cname("attGroup"),
                ],
            ),
            ScriptTemplate::new(
                "despawn npc",
                "DespawnNPCsWithTag",
                vec![ScriptParameter::new_cname("tag")],
            ),
            ScriptTemplate::new(
                "set behavior variable",
                "SetBehaviorVariableQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("variableName"),
                    ScriptParameter::new_f32("variableValue"),
                ],
            ),
            ScriptTemplate::new(
                "broadcast danger",
                "BroadcastDanger",
                vec![
                    ScriptParameter::new_f32_with("lifetime", 10.0),
                    ScriptParameter::new_f32_with("distance", 10.0),
                    ScriptParameter::new_f32_with("interval", 10.0),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "block actor ability",
                "block ability",
                "BlockActorAbility",
                vec![
                    ScriptParameter::new_cname("actorTag"),
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("unBlock", false),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "unblock actor ability",
                "unblock ability",
                "BlockActorAbility",
                vec![
                    ScriptParameter::new_cname("actorTag"),
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("unBlock", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "add npc ability",
                "add ability",
                "ModifyNPCAbilityQuest",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("remove", false),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "remove npc ability",
                "remove ability",
                "ModifyNPCAbilityQuest",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("remove", true),
                ],
            ),
            ScriptTemplate::new(
                "play voiceset",
                "PlayVoicesetQuest",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_string("voiceSet"),
                    ScriptParameter::new_bool_with("oneRandomActor", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "enable npc interactivness",
                "enable interactivness",
                "DisableNPCInteractivness",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_bool_with("disableTalking", false),
                    ScriptParameter::new_bool_with("disableOnliners", false),
                    ScriptParameter::new_bool_with("disableLookats", false),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "disable npc interactivness",
                "disable interactivness",
                "DisableNPCInteractivness",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_bool_with("disableTalking", true),
                    ScriptParameter::new_bool_with("disableOnliners", true),
                    ScriptParameter::new_bool_with("disableLookats", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "enable npc attackable",
                "enable attackable by player",
                "SetNPCIsAttackableByPlayer",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_bool("persistent"),
                    ScriptParameter::new_bool_with("attackable", true),
                    ScriptParameter::new_f32_with("timeout", 10000.0),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "disable npc attackable",
                "disable attackable by player",
                "SetNPCIsAttackableByPlayer",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_bool("persistent"),
                    ScriptParameter::new_bool_with("attackable", false),
                    ScriptParameter::new_f32_with("timeout", 10000.0),
                ],
            ),
            ScriptTemplate::new(
                "enable shopkeeper",
                "EnableShopkeeper",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_bool_with("enable", true),
                ],
            ),
            ScriptTemplate::new(
                "disable shopkeeper",
                "EnableShopkeeper",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_bool_with("enable", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_environment(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("environment..."),
        vec![
            ScriptTemplate::new(
                "change weather",
                "ChangeWeatherQuest",
                vec![
                    ScriptParameter::new_cname("weatherName"),
                    ScriptParameter::new_f32_with("blendTime", 30.0),
                    ScriptParameter::new_bool("randomGen"),
                    ScriptParameter::new_bool("questPause"),
                ],
            ),
            // ScriptTemplate::new(
            //     "deactivate environment",
            //     "DectivateEnvironmentQuest",
            //     vec![ScriptParameter::new_f32_with("blendTime", 20.0)],
            // ),
            ScriptTemplate::new(
                "set timescale",
                "SetTimeScaleQuest",
                vec![ScriptParameter::new_f32_with("timeScale", 1.0)],
            ),
            ScriptTemplate::new(
                "enable illusion",
                "EnableIllusionQuest",
                vec![
                    ScriptParameter::new_cname("illusionTag"),
                    ScriptParameter::new_bool_with("enabled", true),
                ],
            ),
            ScriptTemplate::new(
                "disable illusion",
                "EnableIllusionQuest",
                vec![
                    ScriptParameter::new_cname("illusionTag"),
                    ScriptParameter::new_bool_with("enabled", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_camera(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("camera..."),
        vec![
            ScriptTemplate::new(
                "fade in",
                "FadeInQuest",
                vec![ScriptParameter::new_f32_with("fadeTime", 3.0)],
            ),
            ScriptTemplate::new_with_caption(
                "start cam shake",
                "start shake",
                "CameraShake",
                vec![ScriptParameter::new_f32_with("strength", 1.0)],
            ),
            ScriptTemplate::new_with_caption(
                "stop cam shake",
                "stop shake",
                "StopCameraShake",
                vec![ScriptParameter::new_cname("animName")],
            ),
            ScriptTemplate::new_with_caption(
                "play cam effect",
                "play effect",
                "EffectOnCamera",
                vec![
                    ScriptParameter::new_cname("effectName"),
                    ScriptParameter::new_bool_with("play", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "stop cam effect",
                "stop effect",
                "EffectOnCamera",
                vec![
                    ScriptParameter::new_cname("effectName"),
                    ScriptParameter::new_bool_with("play", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_ntr(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("night to remember..."),
        vec![
            ScriptTemplate::new(
                "set focus highlight",
                "NTR_FocusSetHighlight",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_string_with("highlightType", "FMV_Clue".to_string()),
                    ScriptParameter::new_bool_with("overrideCustomLogic", false),
                ],
            ),
            ScriptTemplate::new(
                "fact checker branch",
                "NTR_FactChecker",
                vec![
                    ScriptParameter::new_string("factName"),
                    ScriptParameter::new_string_with("factCond", "==".to_string()),
                    ScriptParameter::new_i32("val"),
                ],
            ),
            ScriptTemplate::new(
                "play music",
                "NTR_PlayMusic",
                vec![
                    ScriptParameter::new_string_with("areaName", "toussaint".to_string()),
                    ScriptParameter::new_string("eventName"),
                    ScriptParameter::new_string_with("saveType", "SESB_ClearSaved".to_string()),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_facts(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("facts..."),
        vec![
            ScriptTemplate::new(
                "remove fact",
                "RemoveFactQuest",
                vec![ScriptParameter::new_cname("factId")],
            ),
            ScriptTemplate::new(
                "reset fact",
                "ResetFactQuest",
                vec![ScriptParameter::new_cname("factID")],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_hud(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("hud..."),
        vec![
            ScriptTemplate::new(
                "show timelapse",
                "ShowTimeLapse",
                vec![
                    ScriptParameter::new_f32_with("showTime", 5.0),
                    ScriptParameter::new_string("timeLapseMessageKey"),
                    ScriptParameter::new_string("timeLapseAdditionalMessageKey"),
                ],
            ),
            ScriptTemplate::new(
                "show bossfight indicator",
                "ShowBossFightIndicator",
                vec![
                    ScriptParameter::new_bool_with("enable", true),
                    ScriptParameter::new_cname("bossTag"),
                ],
            ),
            ScriptTemplate::new(
                "hide bossfight indicator",
                "ShowBossFightIndicator",
                vec![
                    ScriptParameter::new_bool_with("enable", false),
                    ScriptParameter::new_cname("bossTag"),
                ],
            ),
            // ScriptTemplate::new(
            //     "ShowCompanionIndicator",
            //     "ShowCompanionIndicator",
            //     vec![
            //         enable:bool,
            //         npcTag : name,
            //         optional iconPath : string,
            //         optional npcTag2 : name,
            //         optional iconPath2 : string
            //     ]),
            ScriptTemplate::new(
                "show update info",
                "ForceShowUpdateInfo",
                vec![
                    ScriptParameter::new_string("locKeyText"),
                    ScriptParameter::new_string("locKeyTitle"),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_glossary(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("glossary..."),
        vec![
            ScriptTemplate::new(
                "activate image overlay",
                "EnableGlossaryImageOverrideQuest",
                vec![
                    ScriptParameter::new_cname("uniqueEntryTag"),
                    ScriptParameter::new_string("imageFileName"),
                    ScriptParameter::new_bool_with("enable", true),
                ],
            ),
            ScriptTemplate::new(
                "deactivate image overlay",
                "EnableGlossaryImageOverrideQuest",
                vec![
                    ScriptParameter::new_cname("uniqueEntryTag"),
                    ScriptParameter::new_string("imageFileName"),
                    ScriptParameter::new_bool_with("enable", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_gameplay(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("gameplay..."),
        vec![
            ScriptTemplate::new(
                "enable fasttravel",
                "EnableFastTravelling",
                vec![ScriptParameter::new_bool_with("enable", true)],
            ),
            ScriptTemplate::new(
                "disable fasttravel",
                "EnableFastTravelling",
                vec![ScriptParameter::new_bool_with("enable", false)],
            ),
            ScriptTemplate::new(
                "force dismount",
                "ForceDismount",
                vec![ScriptParameter::new_cname("horseTag")],
            ),
            ScriptTemplate::new(
                "add player ability",
                "ModifyPlayerAbilityQuest",
                vec![
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("remove", false),
                ],
            ),
            ScriptTemplate::new(
                "remove player ability",
                "ModifyPlayerAbilityQuest",
                vec![
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("remove", true),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_items(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("items..."),
        vec![
            ScriptTemplate::new(
                "add alchemy recipe",
                "AddAlchemyRecipeQ",
                vec![ScriptParameter::new_cname("recipeName")],
            ),
            ScriptTemplate::new(
                "add schematics",
                "AddCraftingSchematicsQ",
                vec![ScriptParameter::new_cname("schematicsName")],
            ),
            ScriptTemplate::new_with_caption(
                "disable item",
                "disable",
                "QuestItemDisable",
                vec![
                    ScriptParameter::new_cname("itemName"),
                    ScriptParameter::new_bool_with("addQuestTag", false),
                    ScriptParameter::new_cname("containerTag"),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "remove item",
                "remove",
                "RemoveItemQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("item_name"),
                    ScriptParameter::new_cname("item_category"),
                    ScriptParameter::new_cname("item_tag"),
                    ScriptParameter::new_i32_with("quantity", 1),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "transfer item",
                "transfer",
                "TransferItemQuest",
                vec![
                    ScriptParameter::new_cname("sourceTag"),
                    ScriptParameter::new_cname("targetTag"),
                    ScriptParameter::new_cname("itemName"),
                    ScriptParameter::new_cname("itemCategory"),
                    ScriptParameter::new_cname("itemTag"),
                    ScriptParameter::new_i32_with("quantity", 1),
                ],
            ),
            ScriptTemplate::new(
                "read book",
                "ReadBookByName",
                vec![
                    ScriptParameter::new_cname("bookName"),
                    ScriptParameter::new_bool_with("unread", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_noticeboards(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("noticeboards..."),
        vec![
            ScriptTemplate::new_with_caption(
                "enable noticeboard",
                "enable",
                "DisableNoticeboardQuest",
                vec![
                    ScriptParameter::new_cname("boardTag"),
                    ScriptParameter::new_bool_with("disabled", false),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "disable noticeboard",
                "disable",
                "DisableNoticeboardQuest",
                vec![
                    ScriptParameter::new_cname("boardTag"),
                    ScriptParameter::new_bool_with("disabled", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "open noticeboard",
                "open",
                "ForceOpenNoticeboardQuest",
                vec![
                    ScriptParameter::new_cname("boardTag"),
                    ScriptParameter::new_bool("dontConsiderAsVisiting"),
                    ScriptParameter::new_bool("dontAddDiscoveryFact"),
                    ScriptParameter::new_bool("dontSetPOIEntitiesKnown"),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "remove noticeboard errands",
                "remove errands",
                "RemoveErrandsFromNoticeboard",
                vec![
                    ScriptParameter::new_cname("boardTag"),
                    ScriptParameter::new_string("errandName"),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
// internal helper trait
// ----------------------------------------------------------------------------
trait ParamGenerator {
    fn new_cname(name: &str) -> ScriptParameter;
    fn new_cname_with(name: &str, value: String) -> ScriptParameter;
    fn new_string(name: &str) -> ScriptParameter;
    fn new_string_with(name: &str, value: String) -> ScriptParameter;
    fn new_i32(name: &str) -> ScriptParameter;
    fn new_i32_with(name: &str, value: i32) -> ScriptParameter;
    fn new_f32(name: &str) -> ScriptParameter;
    fn new_f32_with(name: &str, value: f32) -> ScriptParameter;
    fn new_bool(name: &str) -> ScriptParameter;
    fn new_bool_with(name: &str, value: bool) -> ScriptParameter;
}
// ----------------------------------------------------------------------------
impl ParamGenerator for ScriptParameter {
    // ------------------------------------------------------------------------
    fn new_cname(name: &str) -> ScriptParameter {
        ScriptParameter::CName(name.into(), "".into())
    }
    // ------------------------------------------------------------------------
    fn new_cname_with(name: &str, value: String) -> ScriptParameter {
        ScriptParameter::CName(name.into(), value)
    }
    // ------------------------------------------------------------------------
    fn new_string(name: &str) -> ScriptParameter {
        ScriptParameter::String(name.into(), "".into())
    }
    // ------------------------------------------------------------------------
    fn new_string_with(name: &str, value: String) -> ScriptParameter {
        ScriptParameter::String(name.into(), value)
    }
    // ------------------------------------------------------------------------
    fn new_i32(name: &str) -> ScriptParameter {
        ScriptParameter::Int32(name.into(), -1)
    }
    // ------------------------------------------------------------------------
    fn new_i32_with(name: &str, value: i32) -> ScriptParameter {
        ScriptParameter::Int32(name.into(), value)
    }
    // ------------------------------------------------------------------------
    fn new_f32(name: &str) -> ScriptParameter {
        ScriptParameter::Float(name.into(), -1.0)
    }
    // ------------------------------------------------------------------------
    fn new_f32_with(name: &str, value: f32) -> ScriptParameter {
        ScriptParameter::Float(name.into(), value)
    }
    // ------------------------------------------------------------------------
    fn new_bool(name: &str) -> ScriptParameter {
        ScriptParameter::Bool(name.into(), false)
    }
    // ------------------------------------------------------------------------
    fn new_bool_with(name: &str, value: bool) -> ScriptParameter {
        ScriptParameter::Bool(name.into(), value)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
