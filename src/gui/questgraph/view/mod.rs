//
// gui::questgraph::view
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub mod graph;
pub mod menu;
pub mod panels;
pub mod popup;
// ----------------------------------------------------------------------------
#[inline]
pub(in gui) fn show<'ui>(
    ui: &Ui<'ui>,
    screenspace: &ScreenSpaceManager,
    state: &mut GraphState,
    structure: &QuestStructure,
    help: &HelpSystem,
) -> Option<::gui::Action> {
    let mut result = None;

    if let Some(interaction) =
        graph::render_plane(ui, screenspace.main_plane(), &state.plane, &state.edges, &state.blocks)
    {
        result = Some(interaction.into());
    }

    if let Some(interaction) = panels::render(
        ui,
        screenspace.right(),
        &state.selection,
        &mut state.property_editor,
    ) {
        result = Some(interaction.into());
    }

    if let Some(action) = menu::show_context_menus(
        ui,
        &state.menu,
        &state.selection,
        &state.blocks,
        structure,
        &state.config,
    ) {
        result = Some(action.into());
    }

    if help.context_help() {
        show_context_help(ui, screenspace.help(), help, &state.selection, structure);
    }

    result
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::{ImGuiCond, Ui};
use model::shared::LogicOperation;

// actions
use super::{Action, GraphAction, MenuAction};

// state
use super::{
    GraphState, MenuState, PlaneState, QuestStructure, ScriptTemplates, SelectionState, Settings,
};

use super::blocks;
use super::cmds;
use super::popup::SegmentNameRequest;
use super::{BlockId, Blocks, Config, Edges, SegmentId};

// util
use super::help::HelpTopic;
use gui::help::HelpSystem;

use gui::utils::{ScreenSpaceManager, UiArea};

use super::blocks::MAX_SOCKETS as MAX_IN_OUT_BLOCKS;
// ----------------------------------------------------------------------------
fn show_context_help<'ui>(
    ui: &Ui<'ui>,
    area: &UiArea,
    help: &HelpSystem,
    selection: &SelectionState,
    _structure: &QuestStructure,
) {
    ui.window(im_str!("context_help"))
        .show_borders(false)
        .title_bar(false)
        .menu_bar(false)
        .movable(false)
        .resizable(false)
        .collapsible(false)
        .scroll_bar(false)
        .position(area.pos, ImGuiCond::Always)
        .size(area.size, ImGuiCond::Always)
        .always_auto_resize(false)
        .build(|| {
            ui.with_color_var(::imgui::ImGuiCol::Text, (255.0, 255.0, 255.0, 0.4), || {
                ui.text(im_str!("Help"));
                ui.separator();
                ui.child_frame(im_str!("context_help"), (0.0, 0.0))
                    .show_borders(false)
                    .always_show_vertical_scroll_bar(false)
                    .build(|| {
                        let helptopic = match selection.block.as_ref() {
                            Some((_, blockid)) => HelpTopic::from(blockid),
                            None => HelpTopic::Graph,
                        };

                        if let Some(helptxt) = help.get(&helptopic.into()) {
                            ui.text_wrapped(helptxt);
                        }
                    });
            });
        });
}
// ----------------------------------------------------------------------------
