//
// questgraph::view::panels
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn render<'ui>(
    ui: &Ui<'ui>,
    area: &UiArea,
    selection: &SelectionState,
    property_editor: &mut Option<blocks::PropertyEditorState>,
) -> Option<QuestGraphAction> {
    let mut result = None;

    ui.window(im_str!("graph_tools"))
        .show_borders(false)
        .title_bar(false)
        .menu_bar(false)
        .movable(false)
        .resizable(false)
        .collapsible(false)
        .position(area.pos, imgui::ImGuiCond::Always)
        .always_auto_resize(true)
        .always_use_window_padding(true)
        .size_constraints((area.size.0, 0.0), area.size)
        .build(|| {
            if ui.collapsing_header(im_str!("Quest Segments")).build() {
                draw_segment_selection(ui, selection, &mut result);
            }
            if ui.collapsing_header(im_str!("Segment Blocks")).build() {
                draw_block_selection(ui, selection, &mut result);
            }
            if let Some(property_editor) = property_editor.as_mut() {
                if ui.collapsing_header(im_str!("Blocks Properties"))
                    .default_open(true)
                    .build()
                {
                    blocks::view::draw_property_editor(ui, property_editor, &mut result);
                }
            }
        });
    result
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::borrow::Borrow;

use imgui;
use imgui::Ui;

use super::blocks;

// state
use super::SelectionState;

// actions
use super::Action as QuestGraphAction;

// misc
use super::UiArea;
// ----------------------------------------------------------------------------
#[inline]
fn draw_segment_selection<'ui>(
    ui: &Ui<'ui>,
    selection: &SelectionState,
    result: &mut Option<QuestGraphAction>,
) {
    let mut slot = selection.segment.0;
    if ui.list_box2(
        selection
            .segment_list
            .iter()
            .map(|segment| segment.caption.borrow())
            .collect::<Vec<_>>()
            .as_slice(),
        &mut slot,
    ).label(im_str!("##segments"))
        .height_in_items(10)
        .autowidth()
        .build()
    {
        if let Some(selected) = selection.segment_list.get(slot as usize) {
            *result = Some(QuestGraphAction::OnSelectSegment(selected.id.clone()));
        }
    }
}
// ----------------------------------------------------------------------------
#[inline]
fn draw_block_selection<'ui>(
    ui: &Ui<'ui>,
    selection: &SelectionState,
    result: &mut Option<QuestGraphAction>,
) {
    let mut slot = selection
        .block
        .as_ref()
        .map(|&(slot, _)| slot)
        .unwrap_or(-1);

    if ui.list_box2(
        selection
            .block_list
            .iter()
            .map(|block| block.caption.borrow())
            .collect::<Vec<_>>()
            .as_slice(),
        &mut slot,
    ).label(im_str!("##blocks"))
        .height_in_items(10)
        .autowidth()
        .build()
    {
        if let Some(selected) = selection.block_list.get(slot as usize) {
            *result = Some(QuestGraphAction::OnSelectBlock(selected.id.clone()));
        }
    }
}
// ----------------------------------------------------------------------------
