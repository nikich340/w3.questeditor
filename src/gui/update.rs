//
// gui::update - simple(r) actions for updating state, mapping to other actions
// or set of actions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(super) fn handle_menu_selection(
    selection: MenuSelection,
    state: &mut State,
) -> Option<ActionSequence> {
    match selection {
        MenuSelection::ShowHelp => {
            state.windows.show_help = true;
            None
        }
        MenuSelection::ShowContextHelp(enabled) => {
            state.help.enable_context_help(enabled);
            None
        }
        MenuSelection::ShowAbout => {
            state.windows.show_about = true;
            None
        }
        MenuSelection::ShowSettings => {
            state.windows.show_settings = true;
            None
        }
        MenuSelection::LoadProject => Some(ima_seq![
            Action::GuardModifiedData,
            Action::OpenFileBrowser(FileBrowserIntent::LoadProject)
        ]),
        MenuSelection::ReloadProject => {
            Some(ima_seq![Action::GuardModifiedData, Action::ReloadCurrent])
        }
        MenuSelection::SetAutoReload(enabled) => {
            state.data.enable_auto_reload(enabled);
            None
        }

        MenuSelection::SaveProject => Some(ima_seq![Action::SaveCurrent]),

        MenuSelection::SaveProjectAtLocation => Some(ima_seq![Action::OpenFileBrowser(
            FileBrowserIntent::SaveProject
        )]),

        MenuSelection::CloseProject => {
            Some(ima_seq![Action::GuardModifiedData, Action::ResetEditor])
        }

        MenuSelection::NewProject => Some(ima_seq![
            Action::GuardModifiedData,
            Action::CreateNewProject
        ]),

        MenuSelection::Quit => Some(ima_seq![Action::GuardModifiedData, Action::Quit]),

        MenuSelection::QuestGraph(action) => Some(ima_seq![Action::QuestGraph(action)]),
    }
}
// ----------------------------------------------------------------------------
pub(super) fn handle_filebrowser_selection(
    selection: &filebrowser::Selection,
    intent: &FileBrowserIntent,
) -> ActionSequence {
    match selection {
        filebrowser::Selection::Dir(ref dir) => match intent {
            FileBrowserIntent::LoadProject => ima_seq![Action::LoadProject(PathBuf::from(dir))],
            FileBrowserIntent::SaveProject => ima_seq![Action::SaveProject(PathBuf::from(dir))],
        },
    }
}
// ----------------------------------------------------------------------------
//
// ----------------------------------------------------------------------------
use std::path::PathBuf;

use super::filebrowser;
use super::{Action, ActionSequence, FileBrowserIntent, MenuSelection};

use super::State;
// ----------------------------------------------------------------------------
